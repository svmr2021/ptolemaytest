from django.contrib import admin

from core.models.employee import Employee


@admin.register(Employee)
class EmployeeModelAdmin(admin.ModelAdmin):
    list_display = ['fullname', 'id', 'position', 'department']
    list_filter = ['department', 'position']
    search_fields = ['fullname', 'id', 'department__title']