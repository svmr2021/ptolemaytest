from django.contrib import admin

from core.models.department import Department


@admin.register(Department)
class DepartmentModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'id', 'employees_count', 'total_salary']
    search_fields = ['id', 'title', 'employees__fullname']