from ptolemayTest.db.models import BaseModel
from django.db import models


class Employee(BaseModel):
    fullname = models.CharField(max_length=255, db_index=True)
    image = models.ImageField(upload_to='images')
    position = models.CharField(max_length=255)
    salary = models.DecimalField(max_digits=10, decimal_places=2)
    age = models.PositiveIntegerField()
    department = models.ForeignKey('Department', on_delete=models.CASCADE, related_name='employees')

    class Meta:
        ordering = ['-created_at']
        verbose_name_plural = 'Employees'

    def __str__(self):
        return f'{self.fullname} - id({self.id})'
