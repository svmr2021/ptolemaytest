from django.db.models import Sum

from ptolemayTest.db.models import BaseModel
from django.db import models


class Department(BaseModel):
    title = models.CharField(max_length=255, unique=True)

    class Meta:
        ordering = ['-created_at']
        verbose_name_plural = 'Departments'

    def __str__(self):
        return f'{self.title} - id({self.id})'

    @property
    def employees_count(self):
        return self.employees.count()
    
    @property
    def total_salary(self):
        return self.employees.aggregate(total=Sum('salary'))['total']