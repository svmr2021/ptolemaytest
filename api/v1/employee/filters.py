from django_filters import FilterSet, filters

from core.models.employee import Employee


class EmployeeFilter(FilterSet):
    fullname = filters.CharFilter(field_name='fullname', lookup_expr='icontains')

    class Meta:
        model = Employee
        fields = {
            'department__id': ['exact']
        }
