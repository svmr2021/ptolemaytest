from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from api.v1.department.serializers import DepartmentSerializer
from core.models.employee import Employee


class EmployeeSerializer(ModelSerializer):
    image = serializers.ImageField(required=False)

    class Meta:
        model = Employee
        fields = (
            'id',
            'fullname',
            'age',
            'salary',
            'position',
            'department',
            'image'
        )

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['department'] = DepartmentSerializer(instance=instance.department).data
        return data
