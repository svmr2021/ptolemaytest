from rest_framework.routers import DefaultRouter
from .views import EmployeeViewSet

employee_router = DefaultRouter()
employee_router.register("", EmployeeViewSet)