from core.models.employee import Employee
from ptolemayTest.base_test import BaseTestCase
from faker import Faker

faker = Faker()


class EmployeeTest(BaseTestCase):
    def test_employee_create(self):
        department = self._create_department()
        data = {
            "fullname": faker.name(),
            "age": faker.pyint(),
            "salary": 1000,
            "department": department.id,
            "position": 'Developer',
        }
        res = self.client.post(path=f"{self.base_url}/employee/", data=data)
        self.assertEqual(res.status_code, 401)
        self.client.force_authenticate(user=self.user)
        res = self.client.post(path=f"{self.base_url}/employee/", data=data)
        self.assertEqual(res.status_code, 201)

    def test_employee_detail(self):
        department = self._create_department()
        employee = self._create_employee(department=department)
        self.client.force_authenticate(user=self.user)
        res = self.client.get(path=f"{self.base_url}/employee/{employee.id}/")
        self.assertEqual(res.status_code, 200)

    def test_employee_delete(self):
        department = self._create_department()
        employee = self._create_employee(department=department)
        self.client.force_authenticate(user=self.user)
        res = self.client.delete(path=f"{self.base_url}/employee/{employee.id}/")
        self.assertEqual(res.status_code, 204)

    def test_employee_update(self):
        department = self._create_department()
        employee = self._create_employee(department=department)
        self.client.force_authenticate(user=self.user)
        res = self.client.patch(path=f"{self.base_url}/employee/{employee.id}/", data={"fullname" : 'TestName'})
        self.assertEqual(res.status_code, 200)
        employee = Employee.objects.get(id=res.data['id'])
        self.assertEqual(employee.fullname, 'TestName')