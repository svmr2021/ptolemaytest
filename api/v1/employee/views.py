from rest_framework.viewsets import ModelViewSet

from api.v1.employee.filters import EmployeeFilter
from api.v1.employee.serializers import EmployeeSerializer
from core.models.employee import Employee


class EmployeeViewSet(ModelViewSet):
    queryset = Employee.objects.none()
    serializer_class = EmployeeSerializer
    filterset_class = EmployeeFilter

    def get_queryset(self):
        return Employee.objects.all().select_related(
            'department'
        )