from rest_framework.serializers import ModelSerializer

from core.models.department import Department


class DepartmentSerializer(ModelSerializer):
    class Meta:
        model = Department
        fields = (
            'id',
            'title',
            'employees_count',
            'total_salary'
        )