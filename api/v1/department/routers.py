from rest_framework.routers import DefaultRouter
from .views import DepartmentViewSet

department_router = DefaultRouter()
department_router.register("", DepartmentViewSet)