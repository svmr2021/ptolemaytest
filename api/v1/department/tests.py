from core.models.department import Department
from ptolemayTest.base_test import BaseTestCase
from faker import Faker

faker = Faker()


class DepartmentTestCase(BaseTestCase):
    def test_department_create(self):
        data = {
            'title': faker.name()
        }
        res = self.client.post(path=f"{self.base_url}/department/", data=data)
        self.assertEqual(res.status_code, 201)
        self.assertEqual(Department.objects.count(), 1)
        department = Department.objects.get(id=res.data['id'])
        self._create_employee(department=department)
        self._create_employee(department=department)
        res = self.client.get(path=f"{self.base_url}/department/{department.id}/", format='json')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data['employees_count'], 2)
        self.assertEqual(res.data['total_salary'], 2000)

    def test_department_list(self):
        for i in range(5):
            self._create_department()
        res = self.client.get(path=f"{self.base_url}/department/")
        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.data), 5)

    def test_department_detail(self):
        department = self._create_department()
        res = self.client.get(path=f"{self.base_url}/department/{department.id}/")
        self.assertEqual(res.status_code, 200)

    def test_department_update(self):
        department = self._create_department()
        data = {
            'title': faker.name()
        }
        res = self.client.put(path=f"{self.base_url}/department/{department.id}/", data=data)
        self.assertEqual(res.status_code, 200)

    def test_department_delete(self):
        department = self._create_department()
        self.assertEqual(Department.objects.count(), 1)
        res = self.client.delete(path=f"{self.base_url}/department/{department.id}/")
        self.assertEqual(res.status_code, 204)
        self.assertEqual(Department.objects.count(), 0)
