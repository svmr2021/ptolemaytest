from django.db.models import Prefetch
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet

from api.v1.department.serializers import DepartmentSerializer
from core.models.department import Department
from core.models.employee import Employee


class DepartmentViewSet(ModelViewSet):
    queryset = Department.objects.none()
    permission_classes = [AllowAny]
    serializer_class = DepartmentSerializer
    pagination_class = None

    def get_queryset(self):
        return Department.objects.prefetch_related(
            Prefetch('employees', queryset=Employee.objects.all())
        )