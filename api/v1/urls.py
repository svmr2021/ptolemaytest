from django.urls import path, include

from api.v1.department.routers import department_router
from api.v1.employee.routers import employee_router

urlpatterns = [
    path("department/", include(department_router.urls)),
    path("employee/", include(employee_router.urls))
]