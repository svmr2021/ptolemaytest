from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient

from core.models.department import Department
from faker import Faker

from core.models.employee import Employee

faker = Faker()


class BaseTestCase(TestCase):
    base_url = "/api/v1"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def setUp(self):
        self.client = APIClient()
        self.user = self._create_user()

    def _create_user(self):
        user = User.objects.create(username='test@test.com')
        user.set_password(faker.password())
        return user

    def _create_department(self) -> Department:
        data = {
            'title': faker.name()
        }
        return Department.objects.create(**data)

    def _create_employee(self, department: Department) -> Employee:
        data = {
            "fullname": faker.name(),
            "age": faker.pyint(),
            "salary": 1000,
            "department": department,
        }
        return Employee.objects.create(**data)
