## Project description

    Project is done as a test application, and many production features are excluded. Only the REST api part
    is done to show the project's potential. 

## Setup

    You do not need to create user and data initially. There is a db.sqlite3 file, which stores the demo user.
    
    login: test
    password: test

    Note that 'media' folder is pushed to git repo in order to show the dummy data.

Run tests

    python manage.py test

Run server

    python manage.py runserver 8000

Open swagger
    
    http://127.0.0.1:8000/swagger/

Open admin panel
    
    http://127.0.0.1:8000/admin/